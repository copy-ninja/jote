import Vue from "vue";
import Vuex from "vuex";

import VuexPersist from "vuex-persist";

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: "jote",
  storage: window.localStorage
});

export default new Vuex.Store({
  state: {
    notes: [],
    unSavedNoted: [],
    // edit: false,
    // editHeader: false,
    // editContent: false,
    edit: {
      all: false,
      header: false,
      content: false
    },

    settings: {
      bgColor: {
        hex: "#ddd",
        rgba: "red"
      },
      makeBgColorChoise: false,
      showNoteTime: true,
      randomColors: false,
      font: "Text Me One"
    },
    colors: ["red", "blue", "green", "white", "#fc3"]

    // oldIndex: "",
    // newIndex: ""

    // saveNoteSnackbar: false
  },
  mutations: {
    addNote(state, hey) {
      state.notes.unshift(hey);
    },
    rearareng(state, hey) {
      state.notes = hey;
    },
    addUnsavedNoted(state, hey) {
      state.unSavedNoted.push(hey);
    },
    toggleEdit(state, hey) {
      console.log(hey);
      state.edit.header = true;
      state.edit.content = true;

      state.edit.all = hey;
    },
    deleteAllNotes(state, hey) {
      console.log(state.notes);
      // state.notes.splice(0, state.notes.length);
      state.notes.pop();
    },
    changeBgColor(state, hey) {
      state.settings.bgColor = hey;
    },
    toggleBgColorChoise(state, hey) {
      state.settings.makeBgColorChoise = hey;
    },
    toggleShowTime(state, hey) {
      state.settings.showNoteTime = hey;
    },
    toggleRandomColor(state, hey) {
      state.settings.randomColors = hey;
    },
    changeFont(state, hey) {
      state.settings.font = hey;
    },
    restoreDefualt(state, hey) {
      state.settings.showNoteTime = true;
      state.settings.makeBgColorChoise = false;
      state.settings.randomColors = false;
      state.settings.font = "Text Me One";
    },
    randomTrue(state, hey) {
      // var randomColor = Math.floor(Math.random() * this.color.length);
      // if (this.settings.randomColors == true) {
      //   this.colorFn();
      //   val = this.colors[this.colorFn()];
      // }
    }
    // sortBy
    // _oldIndex(state, hey) {
    //   state.oldIndex = hey;
    // },
    // _newIndex(state, hey) {
    //   state.newIndex = hey;
    // }
    // toggelSaveSnackBar(state, hey) {
    //   state.saveNoteSnackbar = hey;
    // }
  },
  actions: {},
  plugins: [vuexLocalStorage.plugin]
});
