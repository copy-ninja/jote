import Vue from "vue";
import "./plugins/axios";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import VueTouch from "vue-touch";
// import "roboto-fontface/css/roboto/roboto-fontface.css";

import "@mdi/font/css/materialdesignicons.css";

// export const serverBus = new Vue();
import VueBus from "vue-bus";
//
import Vue2TouchEvents from "vue2-touch-events";

Vue.use(Vue2TouchEvents);
Vue.use(VueBus);
Vue.use(VueTouch);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
